"""
Filename: main.py
Author: Ronald Markovich
Purpose: python code for the flask server
Date: 12/4/2019
"""
from flask import Flask, render_template, url_for
app = Flask(__name__)

@app.route("/")
@app.route("/select")
def select():
    return render_template('select.html', title='Select')

@app.route("/enter")
def enter():
    return render_template('enter.html', title='Enter Class')

if __name__ == '__main__':
    app.run(debug=True)
